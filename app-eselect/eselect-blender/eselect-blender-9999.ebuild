# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

inherit desktop xdg-utils

if [[ ${PV} == *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://projects.blender.org/ZedDB/${PN}.git"
	KEYWORDS="amd64 arm arm64"
else
	SRC_URI="https://projects.blender.org/ZedDB/${PN}/archive/${PV}.tar.bz2"
	KEYWORDS="amd64 arm arm64"
fi

DESCRIPTION="Eselect module for management of multiple Blender versions"
HOMEPAGE="https://projects.blender.org/ZedDB/${PN}"

LICENSE="GPL-3"
SLOT="0"
IUSE=""

RDEPEND="app-admin/eselect"

src_install() {
	insinto /usr/share/eselect/modules
	doins blender.eselect

	doicon blender.svg
    domenu blender.desktop
}

pkg_postinst() {
	if has_version 'media-gfx/blender'; then
		eselect blender update --if-unset
	fi
	xdg_icon_cache_update
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

