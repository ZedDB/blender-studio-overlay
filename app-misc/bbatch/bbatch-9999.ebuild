# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{10..12} )
inherit distutils-r1

if [[ ${PV} == *9999 ]]; then
	EGIT_REPO_URI="https://projects.blender.org/studio/blender-studio-tools.git"
	inherit git-r3
	KEYWORDS="amd64 x86"
else
	# TODO update url and HOMEPAGE once the repo has been split out of the monorepo
	#SRC_URI="https://projects.blender.org/studio/blender-studio-tools/archive/feature/blender-crawl.tar.gz"
	KEYWORDS="amd64 x86"
fi

DESCRIPTION="blender_crawl is a command line tools to crawl directories for .blend files and execute a provied script"
HOMEPAGE="https://projects.blender.org/studio/blender-studio-tools/src/branch/main/scripts/blender-crawl"

LICENSE="GPL-3"
SLOT="0"

S="${WORKDIR}/${P}/scripts/${PN}"
