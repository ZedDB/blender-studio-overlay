# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Some default mime application file associations"
HOMEPAGE=""

KEYWORDS="amd64"
LICENSE="GPL-3"
SLOT="0"

S="${WORKDIR}"

src_install() {
	# This is the default location for distributions to provide custom mimetype file associations.
	insinto /usr/share/applications/
	newins "${FILESDIR}"/mimeapps.list mimeapps.list
}
