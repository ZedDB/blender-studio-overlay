# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{10..12} )
inherit distutils-r1

if [[ ${PV} == *9999 ]]; then
	EGIT_REPO_URI="https://projects.blender.org/blender/blender-asset-tracer.git"
	inherit git-r3
else
	inherit pypi
	KEYWORDS="amd64 x86"
fi

DESCRIPTION="BAT parses Blend files and produces dependency information"
HOMEPAGE="https://projects.blender.org/blender/blender-asset-tracer"

LICENSE="GPL-2"
SLOT="0"
