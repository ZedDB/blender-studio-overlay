# Copyright 2016-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit edo go-module systemd

DESCRIPTION="Flamenco render farm manager"
HOMEPAGE="https://flamenco.blender.org/"

if [[ ${PV} == *9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://projects.blender.org/studio/flamenco.git"
	EGIT_BRANCH="blender-hq-farm"
	KEYWORDS="amd64"
else
	# TODO the main releases does not bundle all go and nodejs deps yet
	SRC_URI=""
	KEYWORDS="amd64 ~arm ~arm64 ~riscv ~x86"
fi

LICENSE="GPL-3"
SLOT="0"
IUSE="+addon +manager pie +worker"

DEPEND="
	acct-group/flamenco
	acct-user/flamenco
"

RDEPEND="${DEPEND}
	worker? (
		media-video/ffmpeg
	)
"

if [[ ${PV} == *9999 ]]; then
	BDEPEND="
		manager? ( sys-apps/yarn )
	"
fi

DOCS=(
	README.md
)
RESTRICT="test"

src_unpack() {
	if [[ ${PV} == 9999 ]]; then
		git-r3_src_unpack
		cd "${S}" || die
		# Download all go project deps
		ego mod vendor

		if use manager ; then
			# Download all nodejs deps
			echo 'yarn-offline-mirror "./package-cache"' > web/app/.yarnrc
			echo 'yarn-offline-mirror-pruning true' >> web/app/.yarnrc
			edo yarn --cwd web/app install
		fi
	else
		#use verify-sig &&
		#	verify-sig_verify_detached "${DISTDIR}"/${P}.tar.xz{,.sig}
		default
	fi
}

src_compile() {
	GOFLAGS=""
	if use pie ; then
		GOFLAGS+="-buildmode=pie"
	fi

	if use worker ; then
		local flamenco_worker_settings=(
			"-X projects.blender.org/studio/flamenco/internal/appinfo.customHome=${EPREFIX}/var/lib/flamenco_worker/"
			"-X projects.blender.org/studio/flamenco/internal/worker.configFilename=${EPREFIX}/etc/flamenco_worker/config.yaml"
			"-X projects.blender.org/studio/flamenco/internal/worker.credentialsFilename=credentials.yaml"
		)
		local makeenv=(
			LDFLAGS="${flamenco_worker_settings[*]}"
		)
		env "${makeenv[@]}" emake flamenco-worker EXTRA_GOFLAGS="${GOFLAGS}"
	fi

	if use manager ; then
		local flamenco_manager_settings=(
			"-X projects.blender.org/studio/flamenco/internal/manager/config.configFilename=${EPREFIX}/etc/flamenco_manager/config.yaml"
		)
		local makeenv=(
			LDFLAGS="${flamenco_manager_settings[*]}"
		)
		# Needs yarn (and downloaded nodejs deps)
		env "${makeenv[@]}" emake flamenco-manager EXTRA_GOFLAGS="${GOFLAGS}"
	fi
}

src_install() {
	if use addon ; then
		# TODO fix addon install, this will only exist if the manager is compiled
		dodir /usr/share/flamenco/addons/
		unzip web/static/flamenco-addon.zip -d "${ED}//usr/share/flamenco/addons/" || die
	fi
# TODO fix proper restart exit code for the worker and manager and handle them in the service scripts
	if use worker ; then
		dobin flamenco-worker
		newconfd "${FILESDIR}/flamenco-worker.confd" flamenco-worker
		newinitd "${FILESDIR}/flamenco-worker.initd" flamenco-worker

		systemd_newunit "${FILESDIR}"/flamenco-worker.service flamenco-worker.service

		insinto /etc/flamenco_worker
		newins "${FILESDIR}"/worker_conf.yaml config.yaml

		fowners root:flamenco /etc/flamenco_worker/{,config.yaml}
		fperms g+w,o-rwx /etc/flamenco_worker/{,config.yaml}

		diropts -m0750 -o flamenco -g flamenco
		keepdir /var/lib/flamenco_worker
		keepdir /var/log/flamenco
	fi

	if use manager ; then
		dobin flamenco-manager
		newconfd "${FILESDIR}/flamenco-manager.confd" flamenco-manager
		newinitd "${FILESDIR}/flamenco-manager.initd" flamenco-manager

		systemd_newunit "${FILESDIR}"/flamenco-manager.service flamenco-manager.service

		insinto /etc/flamenco_manager
		newins "${FILESDIR}"/manager_conf.yaml config.yaml

		fowners root:flamenco /etc/flamenco_manager/{,config.yaml}
		fperms g+w,o-rwx /etc/flamenco_manager/{,config.yaml}

		diropts -m0750 -o flamenco -g flamenco
		keepdir /var/lib/flamenco_manager
		keepdir /var/log/flamenco
	fi
}
