# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

EGIT_REPO_URI="https://github.com/KDAB/KDDockWidgets.git"

inherit cmake desktop git-r3 xdg

DESCRIPTION="Linux perf GUI for performance analysis"
HOMEPAGE="https://github.com/KDAB/KDDockWidgets"
SRC_URI=""

LICENSE="GPL-2"
KEYWORDS=""
SLOT="0"
IUSE="test"
RESTRICT="test" # fails

RDEPEND="dev-qt/qtbase
	dev-qt/qtsvg:6
	kde-frameworks/extra-cmake-modules
	kde-frameworks/kconfigwidgets:6
	kde-frameworks/kcoreaddons:6
	kde-frameworks/ki18n:6
	kde-frameworks/kio:6
	kde-frameworks/kitemmodels:6
	kde-frameworks/kitemviews:6
	kde-frameworks/kwindowsystem:6
	kde-frameworks/solid:6
	kde-frameworks/threadweaver:6
	dev-util/perf
	virtual/libelf:="
DEPEND="${RDEPEND}"

src_prepare() {
	if ! use test ; then
		sed -i '/add_subdirectory(tests)/d' CMakeLists.txt \
			|| die "sed failed for CMakeLists.txt"
	fi

	cmake_src_prepare
}

src_configure() {
	local mycmakeargs+=(
		-DKDDockWidgets_QT6=yes
	)
	cmake_src_configure
}
