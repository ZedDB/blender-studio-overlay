# Copyright 2017-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

CRATES="
"

inherit cargo

DESCRIPTION="Noise supression using deep filtering "
HOMEPAGE="https://github.com/Rikorose/DeepFilterNet"

if [ ${PV} == "9999" ] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/Rikorose/DeepFilterNet"
else
	SRC_URI="https://github.com/${PN}/${PN}/archive/refs/tags/v${MY_PV}.tar.gz -> ${P}.tar.gz
		${CARGO_CRATE_URIS}"
	KEYWORDS="~amd64 ~arm64 ~ppc64 ~riscv ~x86"
fi

LICENSE="
	Apache-2.0 MIT
"
SLOT="0"

src_unpack() {
	if [[ "${PV}" == *9999* ]]; then
		git-r3_src_unpack
		sed -i -e '/pyDF/d' -e '/demo/d' ${S}/Cargo.toml || die
		cargo_live_src_unpack
	else
		cargo_src_unpack
	fi
}

src_configure() {
	local myfeatures=(
		# These need to be on for the deepfilter binary to be built
		bin tract wav-utils transforms
	)

	cargo_src_configure
}

src_install() {
	dobin "$(cargo_target_dir)/deep-filter"

	exeinto /usr/$(get_libdir)/ladspa
	doexe "$(cargo_target_dir)/libdeep_filter_ladspa.so"

	sed -i "s:/usr/lib:/usr/$(get_libdir):" ladspa/filter-chain-configs/deepfilter-mono-source.conf || die
	sed -i "s:/usr/lib:/usr/$(get_libdir):" ladspa/filter-chain-configs/deepfilter-stereo-sink.conf || die
	insinto /usr/share/deepfilter
	doins ladspa/filter-chain-configs/deepfilter-mono-source.conf
	doins ladspa/filter-chain-configs/deepfilter-stereo-sink.conf
}
