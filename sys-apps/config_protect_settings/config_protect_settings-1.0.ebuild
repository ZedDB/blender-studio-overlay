# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Config Protect settings for the Blender Studio"
HOMEPAGE=""
SRC_URI=""
SLOT="0"
KEYWORDS="amd64"

src_unpack() {
	mkdir ${S}
}

src_install() {
	# For the plasma 5 -> 6 upgrade
	local files=(
	/etc/pam.d/kde
	/etc/xdg/autostart/baloo_file.desktop
	/etc/xdg/autostart/gmenudbusmenuproxy.desktop
	/etc/xdg/autostart/kaccess.desktop
	/etc/xdg/autostart/org.kde.discover.notifier.desktop
	/etc/xdg/autostart/org.kde.plasmashell.desktop
	/etc/xdg/autostart/pam_kwallet_init.desktop
	/etc/xdg/autostart/polkit-kde-authentication-agent-1.desktop
	/etc/xdg/autostart/powerdevil.desktop
	/etc/xdg/autostart/xembedsniproxy.desktop
	)
	echo 'CONFIG_PROTECT_MASK="'${files[@]}'"' > 99blender-studio-settings || die

	doenvd 99blender-studio-settings
}
