# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Scripts for extracting and processing hardware information on linux computers"
HOMEPAGE="https://projects.blender.org/ZedDB/hw_extract"

if [[ ${PV} = *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://projects.blender.org/ZedDB/hw_extract.git"
	KEYWORDS="amd64 arm arm64"
else
	SRC_URI="https://projects.blender.org/ZedDB/hw_extract/archive/v${PV}.tar.xz"
	KEYWORDS="amd64 arm arm64"
fi

LICENSE="GPL-3"
SLOT="0"
IUSE="extras"

RDEPEND="
	sys-apps/dmidecode
	sys-apps/hdparm
	sys-apps/nvme-cli
	sys-apps/edid-decode
"
DEPEND="${RDEPEND}"

src_install() {
	exeinto /usr/bin
	exeopts -m0700
	doexe hw_script.sh || die

	if use extras; then
		doexe parse_output.py
	fi
}
