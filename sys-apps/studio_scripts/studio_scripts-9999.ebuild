# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Scripts for extracting and processing hardware information on linux computers"
HOMEPAGE="https://projects.blender.org/ZedDB/studio_scripts"

if [[ ${PV} = *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://projects.blender.org/ZedDB/studio_scripts.git"
	KEYWORDS="amd64 arm arm64"
else
	SRC_URI="https://projects.blender.org/ZedDB/studio_scripts/archive/v${PV}.tar.xz"
	KEYWORDS="amd64 arm arm64"
fi

LICENSE="GPL-3"
SLOT="0"
IUSE="server"

RDEPEND="
	dev-vcs/git
	net-misc/rsync
	net-misc/iputils
	net-misc/curl
	app-portage/eix
	app-portage/smart-live-rebuild
	app-eselect/eselect-notify-send
"
DEPEND="${RDEPEND}"

src_install() {
	exeinto /usr/bin
	exeopts -m0700
	doexe update_portage_conf.sh || die

	if use server; then
		exeinto /root
		doexe server/*
	else
		doexe client/*
		keepdir /var/db/studio_scripts
	fi
}
