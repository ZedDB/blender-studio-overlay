# Copyright 2022-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

ALTERNATIVES=(
	"blender-9999:media-gfx/blender:9999"
	"blender-bin:media-gfx/blender-bin"
)

inherit app-alternatives desktop

DESCRIPTION="Blender symlinks"
KEYWORDS="amd64"

RDEPEND="
	!app-eselect/eselect-blender
"

src_install() {
	local alt=$(get_alternative)

	dosym "${alt}" /usr/bin/blender
	dosym "${alt}"-thumbnailer /usr/bin/blender-thumbnailer

	make_desktop_entry "blender %f" "Blender" "${alt}" "Graphics;3DGraphics;" \
		"MimeType=application/x-blender;\nPrefersNonDefaultGPU=true\nStartupWMClass=Blender"
	# Rename blender-sym.desktop to blender.desktop. This is so that the appid of the application matches the .desktop file
	mv ${ED}/usr/share/applications/blender-${PN}.desktop ${ED}/usr/share/applications/blender.desktop || die
}
