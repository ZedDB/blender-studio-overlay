# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="3D Creation/Animation/Publishing System"
HOMEPAGE="https://www.blender.org"

if [[ ${PV} = *9999* ]] ; then
	EGIT_LFS="yes"
	inherit git-r3
	EGIT_REPO_URI="https://projects.blender.org/studio/blender-studio-tools.git"
	KEYWORDS="amd64 arm arm64"
else
	SRC_URI=""
	KEYWORDS="amd64 arm arm64"
fi

LICENSE="GPL-3"
SLOT="0"
IUSE=""

RDEPEND="media-gfx/blender"

src_install() {
	dodir /usr/share/blender_studio_tools/addons/
	cp -r scripts-blender/addons/* "${ED}//usr/share/blender_studio_tools/addons/" || die
}
