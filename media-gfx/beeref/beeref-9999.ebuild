# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10..12} )
inherit distutils-r1 virtualx xdg-utils

if [[ ${PV} == *9999 ]]; then
	EGIT_REPO_URI="https://github.com/DarkDefender/beeref.git"
	KEYWORDS="amd64"
	inherit git-r3
else
	inherit pypi
	KEYWORDS="amd64 x86"
fi

DESCRIPTION="A Simple Reference Image Viewer"
HOMEPAGE="https://github.com/rbreu/beeref/"

LICENSE="GPL-3"
SLOT="0"

#httpretty for tests
RDEPEND="
	$(python_gen_cond_dep '
		dev-python/lxml[${PYTHON_USEDEP}]
		dev-python/rectangle-packer[${PYTHON_USEDEP}]
		dev-python/exif[${PYTHON_USEDEP}]
		dev-python/pyqt6[${PYTHON_USEDEP}]
	')
"

BDEPEND="
	$(python_gen_cond_dep '
		test? (
			dev-python/lxml[${PYTHON_USEDEP}]
			dev-python/rectangle-packer[${PYTHON_USEDEP}]
			dev-python/exif[${PYTHON_USEDEP}]
			dev-python/httpretty[${PYTHON_USEDEP}]
			dev-python/pyqt6[${PYTHON_USEDEP}]
			dev-python/pytest-qt[${PYTHON_USEDEP}]
		)
	')
"

distutils_enable_tests pytest

src_prepare() {
    distutils-r1_src_prepare
	#Disable pytest-cov
    sed -i -e '/--cov/d' setup.cfg || die
}

src_test() {
	virtx distutils-r1_src_test
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
