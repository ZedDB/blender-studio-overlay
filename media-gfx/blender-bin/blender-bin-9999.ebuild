# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop xdg-utils pax-utils

DESCRIPTION="Blender Daily Build"
HOMEPAGE="https://builder.blender.org/download"
RESTRICT="strip"

KEYWORDS="amd64"
SLOT="0"
LICENSE="GPL-3"
IUSE=""

RDEPEND="
media-libs/libglvnd
sys-libs/glibc
virtual/libcrypt
x11-base/xorg-server
"

BDEPEND="
	app-misc/jq
"

PROPERTIES+=" live"

src_unpack() {
	if [[ -z "${BLENDER_BIN_URL}" ]]; then
		latest=$(wget -O - "$HOMEPAGE/daily/?format=json&v=1" | \
			jq -r 'map(select(.platform == "linux" and .branch == "main" and .file_extension == "xz")) | .[0].url' )
	else
		latest=$BLENDER_BIN_URL
	fi

	echo $latest

	wget -c "$latest" -O "${T}/blender_daily.tar.xz" || die
	mkdir -p "${S}"
	tar xvf "${T}/blender_daily.tar.xz" --directory "${S}" --strip-components=1 || die
}

src_install() {
	declare BLENDER_OPT_HOME=/opt/${PN}

	# Prepare icons and .desktop for menu entry
	mv blender.desktop ${PN}.desktop
	mv blender.svg ${PN}.svg
	mv blender-symbolic.svg ${PN}-symbolic.svg
	sed -i -e "s:=blender:=${PN}:" -e "s:Blender:Blender Bin:" "${PN}.desktop" || die

	# Install icons and .desktop for menu entry
	doicon -s scalable ${S}/blender*.svg
	domenu ${PN}.desktop

	# Install all the blender files in /opt
	dodir ${BLENDER_OPT_HOME%/*}
	mv "${S}" "${D}"${BLENDER_OPT_HOME} || die

	# Create symlink /usr/bin/blender-bin
	dosym ${BLENDER_OPT_HOME}/blender /usr/bin/${PN}

	# revdep-rebuild entry
	#insinto /etc/revdep-rebuild
	#doins "${FILESDIR}"/10${PN} || die

	# Required in order to use plugins and even run firefox on hardened.
	pax-mark mr "${ED}"/${BLENDER_OPT_HOME}/{blender,blender-softwaregl,blender-thumbnailer}
}

pkg_postinst() {
	xdg_icon_cache_update
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_icon_cache_update
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}
