# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop xdg-utils

DESCRIPTION="Extra icons for Blender desktop shortcuts"
HOMEPAGE="https://www.blender.org"

KEYWORDS="amd64 arm arm64"

LICENSE="|| ( GPL-3 BL )"
SLOT=0
S=${WORKDIR}

svg_to_greyscale() {
  file=$1

  # Get all regular hex color strings. IE #123456
  hex_colors=$(egrep -o '"#[0-9a-f]{6}"' $file)

  # For hex color sequences with only three numbers. IE #c2a
  hex_colors_3=$(egrep -o '"#[0-9a-f]{3}"' $file)

  convert_rgb_to_greyscale() {
    red=$1
    green=$2
    blue=$3

    # Convert to grey scale colors by extracting the Y component of YUV.
    # https://en.wikipedia.org/wiki/YUV#Conversion_to.2Ffrom_RGB
    Y=$(echo $((0x$red)) "* 0.299" + $((0x$green)) "* 0.587" + $((0x$blue)) "* 0.114" | bc -l)
    # Remove decimals
    Y=${Y%%.*}

    # Convert back to RGB color hex string
    printf '#%x%x%x\n' $Y $Y $Y
  }

  for entry in $hex_colors; do
    # Remove hash symbol and quotation marks
    color="${entry:2:-1}"
    # Separete out all the colors
    red=${color:0:2}
    green=${color:2:2}
    blue=${color:4:2}

    new_color=$(convert_rgb_to_greyscale $red $green $blue)
    new_color='"'$new_color'"'
    sed -i "s/$entry/$new_color/g" $file
  done

  for entry in $hex_colors_3; do
    # Remove hash symbol and quotation marks
    color="${entry:2:-1}"
    # Separete out all the colors
    red=${color:0:1}
    green=${color:1:1}
    blue=${color:2:1}

    new_color=$(convert_rgb_to_greyscale $red$red $green$green $blue$blue)
    new_color='"'$new_color'"'
    sed -i "s/$entry/$new_color/g" $file
  done
}

src_prepare() {
    cp ${FILESDIR}/* ${S}
    cd ${S}
	mv blender.svg blender_grey.svg
    svg_to_greyscale blender_grey.svg
	default
}

src_install() {
	doicon -s scalable *.svg
}

pkg_postinst() {
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_icon_cache_update
}
