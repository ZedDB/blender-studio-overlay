# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop

DESCRIPTION="3D Creation/Animation/Publishing System"
HOMEPAGE="https://www.blender.org"

if [[ ${PV} = *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://projects.blender.org/studio/blender-studio-tools.git"
	KEYWORDS="amd64 arm arm64"
else
	SRC_URI=""
	KEYWORDS="amd64 arm arm64"
fi

LICENSE="GPL-3"
SLOT="0"
IUSE=""

RDEPEND="media-gfx/blender-bin"

S="${WORKDIR}/${P}/application-templates/${PN}"

src_prepare() {
	sed -e "s|Exec=/path/to/blender_dir/blender|Exec=env BLENDER_USER_SCRIPTS=/usr/share/blender_media_viewer/ blender-bin|" -i blender_media_viewer.desktop || die
	sed -e "s|Icon=/path/to/blender_dir/blender.svg|Icon=blender-bin|" -i blender_media_viewer.desktop || die
	sed -e "s|Terminal=true|Terminal=false|" -i blender_media_viewer.desktop || die

	default
}

src_install() {
	dodir /usr/share/blender_media_viewer/startup/bl_app_templates_user/
	cp -R blender_media_viewer "${ED}//usr/share/blender_media_viewer/startup/bl_app_templates_user/" || die

	domenu blender_media_viewer.desktop
}
