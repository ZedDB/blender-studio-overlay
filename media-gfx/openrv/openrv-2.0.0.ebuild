# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10..12} )
inherit cmake python-single-r1 desktop

if [[ ${PV} == *9999 ]]; then
	EGIT_REPO_URI="https://github.com/AcademySoftwareFoundation/OpenRV.git"
	KEYWORDS="amd64"
	inherit git-r3
else
	KEYWORDS="amd64"
	SRC_URI="
	https://github.com/AcademySoftwareFoundation/OpenRV/archive/refs/tags/v${PV}.tar.gz
    https://github.com/shotgunsoftware/openrv-pub/archive/e1a849d089f4bae233285ece2b6e0db39d040f8e.zip
	https://github.com/shotgunsoftware/openrv-WFObj/archive/648355e8ce65e961892d6f4c1f81a5a79432eff0.zip
	"
	S="${WORKDIR}/OpenRV-${PV}"
fi

DESCRIPTION="A Simple Reference Image Viewer"
HOMEPAGE="https://github.com/AcademySoftwareFoundation/OpenRV"

LICENSE="Apache-2.0"
SLOT="0"
IUSE="debug"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

#pyopengl is for the pyside2 test plugin
RDEPEND="${PYTHON_DEPS}
	$(python_gen_cond_dep '
		dev-python/six[${PYTHON_USEDEP}]
		dev-python/requests[${PYTHON_USEDEP}]
		dev-python/pyside2[${PYTHON_USEDEP},designer,positioning,quick,qml,webchannel,webengine]
		dev-python/pyopengl[${PYTHON_USEDEP}]
	')

	dev-qt/qtcore
	dev-qt/qtgui
	dev-qt/qtnetwork
	dev-qt/qtopengl
	dev-qt/qtdeclarative:5
	dev-qt/qtsvg:5
	dev-qt/qttest
	dev-qt/qtwebchannel:5
	dev-qt/qtwebengine:5
	dev-qt/qtwidgets
	dev-qt/qtxml
	dev-qt/qtxmlpatterns

    media-libs/mesa[osmesa]
	dev-libs/spdlog
	dev-libs/boehm-gc[cxx]
	media-libs/lcms

	dev-libs/imath
	dev-lang/python
	media-libs/openexr
	media-libs/libpng
	media-libs/opencolorio
	media-libs/openimageio
	sys-libs/zlib
	media-libs/glew
	media-libs/libjpeg-turbo
	>=media-video/ffmpeg-6.1.1
	media-libs/tiff
	media-libs/libraw
	dev-cpp/yaml-cpp

	dev-libs/libaio
"

BDEPEND="${RDEPEND}"

PATCHES="
	${FILESDIR}/config_pass.patch
	${FILESDIR}/fix_compile.patch
	${FILESDIR}/python.patch
	${FILESDIR}/fix_ffmpeg_compile.patch
	${FILESDIR}/fix_ffmpeg_playback.patch
	${FILESDIR}/qt_rc.patch
	${FILESDIR}/dont_install_wrappers.patch
	${FILESDIR}/relative_libs.patch
	${FILESDIR}/force_qpa_platform.patch
	${FILESDIR}/no_nvidia_check.patch
	${FILESDIR}/fix_cms.patch
	${FILESDIR}/allow_gentoo_build.patch
	${FILESDIR}/fix_non_git_build.patch
"

pkg_setup() {
	python-single-r1_pkg_setup
}

src_unpack() {
	if [[ ${PV} = *9999* ]] ; then
		git-r3_src_unpack

	else
		default
		cp -r openrv-pub-*/* ${S}/src/pub/ || die
		cp -r openrv-WFObj-*/* ${S}/src/lib/files/WFObj/ || die
	fi
}

src_configure() {
	local mycmakeargs=(
		-DBUILD_TESTING=OFF
		-DGIT_SUBMODULE=OFF
		-DFREETYPE_INCLUDE_DIR_ft2build="/usr/include/freetype2"
		-DCMAKE_INSTALL_PREFIX="${ED}/opt/openrv"
		-DPython_INCLUDE_DIR="$(python_get_includedir)"
		-DPython_LIBRARY="$(python_get_library_path)"
	)

	append-cflags $(usex debug '-DDEBUG' '-DNDEBUG')
	append-cppflags $(usex debug '-DDEBUG' '-DNDEBUG')

	cmake_src_configure
}

src_install() {
	cmake_src_install

	newicon ${ED}/opt/openrv/resources/RV.ico openrv.ico
	make_desktop_entry "/opt/openrv/bin/rv" "OpenRV" /usr/share/pixmaps/openrv.ico "AudioVideo;Viewer;Player;"
}

pkg_postinst() { xdg_icon_cache_update; }
pkg_postrm() { xdg_icon_cache_update; }

#Multiple libs in src/pub seems to not be needed like lcms (as we can use the sys libs)

# Perhaps remove all qt.conf files so I don't need to set QT_PLUGIN_PATH=/usr/lib64/qt5/plugins/

#LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/openrv/lib/ QT_PLUGIN_PATH=/usr/lib64/qt5/plugins/ PYTHONPATH="${PYTHONPATH}:/opt/openrv/plugins/Python" QT_QPA_PLATFORM=xcb /opt/openrv/bin/rv.bin

# TODO python fails to initialize properly without the right pyside2 useflags
# however there is no indication from openrv when opening that it is pyside2 that is the issue
# it should error out is a better way. Preferably at cmake configure time

# fix png files with "pngfix"
# opt/openrv/plugins/SupportFiles/session_manager/channel.png
# opt/openrv/plugins/SupportFiles/session_manager/layer.png
# opt/openrv/plugins/SupportFiles/session_manager/view.png
