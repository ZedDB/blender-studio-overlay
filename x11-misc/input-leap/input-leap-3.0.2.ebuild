# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit virtualx xdg cmake

DESCRIPTION="Share a mouse and keyboard between computers (fork of Barrier)"
HOMEPAGE="https://github.com/input-leap/input-leap"
if [ ${PV} == "9999" ] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/input-leap/input-leap.git"
else
	SRC_URI="https://github.com/${PN}/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~arm64 ~ppc64 ~riscv ~x86"
fi

LICENSE="GPL-2"
SLOT="0"
IUSE="+gui test X wayland"
RESTRICT="!test? ( test )"

RDEPEND="
	net-misc/curl
	X? (
		x11-libs/libICE
		x11-libs/libSM
		x11-libs/libX11
		x11-libs/libXext
		x11-libs/libXi
		x11-libs/libXinerama
		x11-libs/libXrandr
		x11-libs/libXtst
	)
	gui? (
		dev-qt/qtbase:6[gui,network,widgets]
		net-dns/avahi[mdnsresponder-compat]
	)
	dev-libs/openssl:0=
	wayland? (
		>=dev-libs/libportal-0.8.0
		dev-libs/libei
	)
"
DEPEND="
	${RDEPEND}
	dev-cpp/gtest
	X? ( x11-base/xorg-proto )
"
BDEPEND="
	virtual/pkgconfig
	gui? ( dev-qt/qttools:6[linguist] )
"

DOCS=(
	ChangeLog
	README.md
	doc/${PN}.conf.example{,-advanced,-basic}
)

PATCHES=(
	"${FILESDIR}/${PN}-3.0.2-configure_segfault.patch"
)

src_configure() {
	local mycmakeargs=(
		-DINPUTLEAP_BUILD_GUI=$(usex gui)
		-DINPUTLEAP_BUILD_TESTS=$(usex test)
		-DINPUTLEAP_USE_EXTERNAL_GTEST=ON
		-DINPUTLEAP_BUILD_LIBEI=$(usex wayland)
		-DINPUTLEAP_BUILD_X11=$(usex X)
	)

	cmake_src_configure
}

src_test() {
	"${BUILD_DIR}"/bin/unittests || die
	virtx "${BUILD_DIR}"/bin/integtests || die
}
