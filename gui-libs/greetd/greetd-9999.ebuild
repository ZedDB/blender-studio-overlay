# Copyright 2017-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

CRATES=""

inherit cargo optfeature pam systemd

DESCRIPTION="ipc based login daemon"
HOMEPAGE="https://git.sr.ht/~kennylevinsen/greetd/"

if [ ${PV} == "9999" ] ; then
	inherit git-r3
	EGIT_BRANCH="autologin"
	EGIT_REPO_URI="https://github.com/DarkDefender/${PN}"
else
	SRC_URI="https://git.sr.ht/~kennylevinsen/greetd/archive/${PV}.tar.gz -> ${P}.tar.gz
		$(cargo_crate_uris ${CRATES})
	"
fi

LICENSE="GPL-3+"
# Dependent crate licenses
LICENSE+=" Apache-2.0 MIT Unicode-DFS-2016 Unlicense"
SLOT="0"
KEYWORDS="amd64 ~arm64 ~ppc64 ~riscv ~x86"
IUSE="man"

DEPEND="
	acct-user/greetd
	sys-auth/pambase
	sys-libs/pam
"
RDEPEND="${DEPEND}"
BDEPEND="man? ( app-text/scdoc )"

QA_FLAGS_IGNORED="usr/bin/.*greet.*"

PATCHES=(
	"${FILESDIR}/${PN}-0.6.1-correct_user_config_toml.patch"
)

src_unpack() {
	if [[ "${PV}" == *9999* ]]; then
		git-r3_src_unpack
		cargo_live_src_unpack
	else
		cargo_src_unpack
	fi
}

src_compile() {
	cargo_src_compile
	if use man; then
		scdoc < ./man/agreety-1.scd > ./agreety.1 || die
		scdoc < ./man/greetd-1.scd > ./greetd.1 || die
		scdoc < ./man/greetd-5.scd > ./greetd.5 || die
		scdoc < ./man/greetd-ipc-7.scd > ./greetd-ipc.7 || die
	fi
}

src_install() {
	dobin "$(cargo_target_dir)"/{agreety,fakegreet,greetd}

	insinto /etc/greetd
	doins config.toml

	systemd_dounit greetd.service

	if use man; then
		doman agreety.1 greetd.1 greetd.5 greetd-ipc.7
	fi

	newpamd - greetd <<-EOF
		# newer greetd errors when no greetd-specific pam.d config is
		# available
		# workaround by just using the fallback that it was already
		# using anyway
		auth            include         login
		account         include         login
		password        include         login
		session         include         login
	EOF
}

pkg_postint() {
	optfeature "eye-candy gtk based greeter" gui-apps/gtkgreet
	optfeature "simplistic but sleek terminal greeter" gui-apps/tuigreet
}
