# Copyright 2022-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop xdg-utils

MY_PV="${PV/_/-}"
# Detect if we are a stable release or not
[[ ${PV} = ${MY_PV} ]] && MY_PV="${PV}-stable"
DESCRIPTION="Multi-platform 2D and 3D game engine with a feature-rich editor"
HOMEPAGE="https://godotengine.org/"
SRC_URI="
	https://github.com/godotengine/godot-builds/releases/download/${MY_PV}/Godot_v${MY_PV}_linux.x86_64.zip
	https://raw.githubusercontent.com/godotengine/godot/refs/heads/master/icon_outlined.svg
"

LICENSE="
	MIT
	Apache-2.0 BSD Boost-1.0 CC0-1.0 Unlicense ZLIB
	CC-BY-4.0 OFL-1.1
"
SLOT="0"
KEYWORDS="~amd64"

S="${WORKDIR}"

src_install() {
	# Install binary
	newbin Godot_v${MY_PV}_linux.x86_64 ${PN} || die
	# Rename icon
	newicon -s scalable "${DISTDIR}"/icon_outlined.svg ${PN}.svg
	make_desktop_entry "${PN}" "Godot" "${PN}" "Graphics" "StartupWMClass=Godot;"
}

pkg_postinst() {
	xdg_icon_cache_update
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_icon_cache_update
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}
