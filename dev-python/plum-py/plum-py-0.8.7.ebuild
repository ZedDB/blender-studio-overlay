# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYPI_NO_NORMALIZE=1
PYTHON_COMPAT=( python3_{10..12} )
inherit distutils-r1 pypi

DESCRIPTION="Provides classes and utility functions to transform byte sequences into Python objects and back"
HOMEPAGE="
    https://pypi.org/project/plum-py/
    https://gitlab.com/dangass/plum
"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~arm64 x86"
RESTRICT="test"

# Need the python baseline package for the tests
#distutils_enable_tests pytest
