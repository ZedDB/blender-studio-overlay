# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10..12} )
inherit distutils-r1 flag-o-matic

if [[ ${PV} == *9999 ]]; then
	EGIT_REPO_URI="https://github.com/Penlect/rectangle-packer.git"
	inherit git-r3
else
	PYPI_NO_NORMALIZE=1
	inherit pypi
	KEYWORDS="amd64 x86"
fi

DESCRIPTION="Pack a set of rectangles into a bounding box with minimum area"
HOMEPAGE="https://github.com/Penlect/rectangle-packer"

LICENSE="MIT"
SLOT="0"

BDEPEND="
	$(python_gen_cond_dep '
		dev-python/cython[${PYTHON_USEDEP}]
	')
"

PATCHES=(
	"${FILESDIR}"/fix_cython_comp.patch
)


distutils_enable_tests unittest

src_configure() {
	# Disable debug asserts
	append-cppflags -DNDEBUG

	distutils-r1_src_configure
}
