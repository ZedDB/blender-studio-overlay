# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10..12} )
inherit distutils-r1 flag-o-matic

if [[ ${PV} == *9999 ]]; then
	EGIT_REPO_URI="https://github.com/Penlect/rectangle-packer.git"
	inherit git-r3
else
	#PYPI_NO_NORMALIZE=1
	inherit pypi
	KEYWORDS="amd64 x86"
fi

DESCRIPTION="Fake Blender Python API module collection for the code completion."
HOMEPAGE="https://github.com/nutti/fake-bpy-module"

LICENSE="MIT"
SLOT="0"
