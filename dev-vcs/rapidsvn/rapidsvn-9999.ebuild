# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

WX_GTK_VER="3.0-gtk3"
inherit autotools desktop wxwidgets xdg-utils git-r3

DESCRIPTION="Cross-platform GUI front-end for the Subversion revision system"
HOMEPAGE="https://github.com/RapidSVN/RapidSVN"

EGIT_REPO_URI="https://github.com/RapidSVN/RapidSVN.git"

LICENSE="GPL-2 LGPL-2.1 FDL-1.2"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="doc"

RDEPEND="
	dev-libs/apr
	dev-libs/apr-util
	dev-vcs/subversion
	x11-libs/wxGTK:${WX_GTK_VER}[X]"
DEPEND="${RDEPEND}
	doc? (
		dev-libs/libxslt
		app-text/docbook-sgml-utils
		app-doc/doxygen
		app-text/docbook-xsl-stylesheets )"

DOCS=( HACKING.txt TRANSLATIONS )

src_prepare() {
	default

	eautoreconf
}

src_configure() {
	setup-wxwidgets

	local myconf=( --with-wx-config=${WX_CONFIG} )

	if use doc; then
		myconf+=( --with-manpage=yes )
	else
		myconf+=(
				--without-xsltproc
				--with-manpage=no
				--without-doxygen
				--without-dot )
	fi

	econf "${myconf[@]}"
}

src_install() {
	default

	doicon rapidsvn/res/rapidsvn.ico
	make_desktop_entry rapidsvn "RapidSVN" \
		"rapidsvn.ico" \
		"RevisionControl;Development"

	if use doc ; then
		doman doc/manpage/${PN}.1
		dohtml "${S}"/doc/svncpp/html/*
	fi
}
